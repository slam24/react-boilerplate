## Installation

``` bash
# clone the repo
$ git clone https://github.com/slam24/react-bolierplate.git

# go into app's directory
$ cd react-bolierplate

# install app's dependencies for server
$ npm install or yarn install
```

## Usage

``` bash
# serve with hot reload at localhost:3000.
$ npm start

# production release
$ npm run production
```